<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'CvControlleur@home')->name('home');

Route::get('/home', 'CvControlleur@home')->name('home');
Route::get('/formation', 'CvControlleur@formation')->name('formation');
Route::get('/experience', 'CvControlleur@experience')->name('experience');
Route::get('/hobbies', 'CvControlleur@hobbies')->name('hobbies');
Route::get('/admin', 'CvAdminController@admin')->name('admin');

Route::resources([
    'admin/formation' => 'AdminFrmController',
    'admin/experience' => 'AdminXpController',
    'admin/hobbies' => 'AdminHobController'
]);