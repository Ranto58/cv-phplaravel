@extends('admin.template') @section('titre') Mon CV @endsection @section('content')
<div class="text-center">
    <h1>Gestion des hobbies</h1>
</div>

@foreach($hobbies as $hobby)
<div class="container mt-3">
    <div class="row  mb-2 d-flex align-items-center">
        <div class="col py-4 px-lg-5 border bg-light">{{$hobby->name}}
        </div>
        <div class="d-flex ml-2" style="height: 40px;">
            <a class="btn btn-primary" href='/admin/hobbies/edit' role="button">
                <i class="fas fa-pen"></i>
            </a>
            <a class="btn btn-primary ml-2" href='/admin/hobbies/delete' role="button">
                <i class="fas fa-trash-alt"></i>
            </a>
        </div>
    </div>
</div>
@endforeach

<div class="row mt-3">
    <div class="mx-auto">
        <a class="btn btn-primary" href='/admin/hobbies/create' role="button">
            <i class="fas fa-plus-circle"></i>
        </a>

        <div>
        </div>
        @endsection