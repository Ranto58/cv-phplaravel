<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 
use App\Experience;

class AdminXpController extends Controller
{
    /**
     * Display a listing of the resource.w
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experiences = DB::table('experiences')->get();
        return view('admin.experience', ['experiences' => $experiences]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/create/experience');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->except('_token');

        $experience = new experience();

        $experience->date = $request->input('date');
        $experience->name = $request->input('name');
        $experience->type = $request->input('type');
        $experience->description = $request->input('description');
        $experience->save();

        return redirect('/admin/experience');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin/update/experience');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $experience = Experience::find($id);
        return view('experience.edit', ['experience' => $experience]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token', '_method');
        $experience = Experience::find($id);
        foreach($inputs as $key => $value){
            $experience->$key = $value;
        }
        $experience->save();

        return redirect('/admin/experience');
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
       // $request->validate([
        //     'date'=>'required',
        //     'name' =>'required',
        //     'type' => 'required',
        //     'description' => 'required'
        // ]);

        // $experience->date = $request->get('date');
        // $experience->name = $request->get('name');
        // $experience->type = $request->get('type');
        // $experience->description = $request->get('description');
