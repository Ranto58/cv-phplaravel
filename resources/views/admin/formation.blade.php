@extends('admin.template')

@section('titre')
Mon CV
@endsection

@section('content')
<div class="text-center">
<h1>Gestion des formations</h1>
</div>
@foreach($formations as $formation)

    <div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$formation->date}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$formation->name}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$formation->description}}</div>
            <div class="d-flex ml-2 mt-3" style="height: 40px;">
            <a class="btn btn-primary" href='/admin/formation/edit' role="button">
                <i class="fas fa-pen"></i>
            </a>
            <a class="btn btn-primary ml-2" href='/admin/formation/delete' role="button">
                <i class="fas fa-trash-alt"></i>
            </a>
        </div>  
      </div>
    </div>
@endforeach
    <div class="row mt-3">
      <div class="mx-auto">
        <a class="btn btn-primary" href='/admin/formation/create' role="button">
          <i class="fas fa-plus-circle"></i>
        </a>
      <div>
    </div>
@endsection
