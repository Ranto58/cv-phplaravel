@extends('template')

@section('titre')
Mon CV
@endsection

@section('content')
@foreach($formations as $formation)

    <div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$formation->date}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$formation->name}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$formation->description}}</div>

        </div>
    </div>
@endforeach
@endsection
