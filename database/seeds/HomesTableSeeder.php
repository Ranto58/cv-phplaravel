<?php

use Illuminate\Database\Seeder;

class HomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homes')->insert(
            array(
                array(
                    'name' => 'Rakotomalala Rantoniaina Antonio',
                    'description' => 'B2 en Informatique à Ynov Toulouse',
                    'age' => '21 ans',
                    'mail' => 'rantoniainaantonio.rakotomalala@ynov.com',
                    'phone' => '07-76-72-60-76',
                ),
            )
        );
    }
}
