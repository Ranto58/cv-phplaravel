@extends('template')

@section('titre')
Mon CV
@endsection
@section('content')
@foreach($homes as $home)
<div class="jumbotron mb-0">
  <h1 class="display-4">Bienvenue sur mon CV</h1>
    <div class="text-center mt-5">
      <img src="moi.jpg" class="rounded-circle" alt="Antonio" style="width:170px">
    </div>
    <p class="lead">Je m'appelle<b><a href="https://www.antonio-cv.yo.fr" target="_blank" style="color:black; text-decoration:none"> {{$home->name}} </a></b> et je suis en <b>{{$home->description}}</b> <i class="fab fa-creative-commons-sampling"></i> .</p>
  <hr class="my-4">
    <p>
      Age <i class="fas fa-pager"></i> : {{$home->age}} 
    </p>
    <p>
      Mail <i class="fab fa-mailchimp"></i> : <a href="mailto:rantoniainaantonio.rakotomalala@ynov.com">{{$home->mail}}</a>
    </p>
    <p>
      Téléphone <i class="fas fa-phone"></i> : <a href="tel:07-76-72-60-76">{{$home->phone}}</a>
    </p>
  </div>
</div>
@endforeach

@endsection('content')
