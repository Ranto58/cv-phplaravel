<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">     
    <title>
    @yield('titre')
    </title>


</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="../home">Antonio - CV</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link {{ Request::is('admin/formation') ? 'active' : '' }}" href="/admin/formation">Formations
            </a>
            <a class="nav-item nav-link {{ Request::is('admin/experience') ? 'active' : '' }}" href="/admin/experience">Experiences</a>
            <a class="nav-item nav-link {{ Request::is('admin/hobbies') ? 'active' : '' }}" href="/admin/hobbies">Hobbies</a>
            <a class="nav-item nav-link" href="/hobbies"><i class="fas fa-sign-out-alt"></i></a>

        </div>
    </div>
</nav>
<div style="width: 100%; height: 6px; background: #acf"></div>
@yield('content')

</body>
</html>