@extends('admin.template')

@section('titre')
Mon CV
@endsection

@section('content')    
<div class="text-center">
<h1>Gestion des expériences</h1>
</div>
@foreach($experiences as $experience)

<div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$experience->date}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->name}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->type}}
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->description}}</div>
        </div>
        <div class="d-flex ml-2" style="height: 40px;">
            <a class="btn btn-primary" href="/admin/experience/edit" role="button">
                <i class="fas fa-pen"></i>
            </a>
            <a class="btn btn-primary ml-2" href='/admin/experience/delete' role="button">
                <i class="fas fa-trash-alt"></i>
            </a>
          </div>  
      </div>
    </div>
@endforeach
    <div class="row">
      <div class="mx-auto">
        <a class="btn btn-primary" href='/admin/experience/create' role="button">
          <i class="fas fa-plus-circle"></i>
        </a>
      <div>
    </div>
@endsection
