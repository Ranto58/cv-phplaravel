@extends('template')

@section('titre')
Mon CV
@endsection

@section('content')
<form  method="POST" action="/admin/formation">
@csrf

  <div class="row mt-5">
    <div class="col ml-3">
      <input type="text" class="form-control" name="date" placeholder="date">
    </div>
    <div class="col mr-3">
      <input type="text" class="form-control" name="name" placeholder="Formation Name">
    </div>
    <div class="form-group">
    <textarea class="form-control" rows="3" name="description" placeholder="Description"></textarea>
  </div>
    <div class="col mr-3">
    <button type="submit" class="btn btn-primary mb-2">Créer</button>

  </div>
  </div>
</form>
@endsection
