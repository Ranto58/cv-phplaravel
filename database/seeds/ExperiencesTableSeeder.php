<?php

use Illuminate\Database\Seeder;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experiences')->insert(
                  array(
                      array(
                          'date' => '2020',
                          'name' => 'Unknown',
                          'type' => 'Stage',
                          'description' => 'Unknown'
                      ),
                  )
              );
    }
}
