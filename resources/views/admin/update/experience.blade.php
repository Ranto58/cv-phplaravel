@extends('template')

@section('titre')
Mon CV
@endsection

@section('content')
<form method="POST" action="/admin/experience">
@csrf
@method("PUT")

<div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center"></div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4"></div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">
            <div class="col py-3 px-lg-5 border bg-light text-center py-4"></div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary mb-2" name="submit" style="margin-left:50%">Submit</button>
  </div>
</form>
@endsection