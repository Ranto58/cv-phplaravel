<?php

use Illuminate\Database\Seeder;

class FormationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formations')->insert(
            array(
                array(
                    'date' => '2014 - 2015',
                    'name' => 'BACC - S',
                    'description' => 'Mathématiques - physiques',
                ),
            )
        );
    }
}
