@extends('template')

@section('titre')
Mon CV
@endsection

@section('content')
<form method="POST" action="/admin/hobbies">
  @csrf
  <div class="row mt-5 w-50 mx-auto">
    <div class="col">
      <input type="text" class="form-control" name="name" placeholder="your hobbies">
    </div>
    <div class="col mr-3">
    <button type="submit" class="btn btn-primary mb-2">Créer</button>

  </div>
  </div>
</form>
@endsection