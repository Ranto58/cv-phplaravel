@extends('template')

@section('titre')
Mon CV
@endsection

@section('content')

@foreach($experiences as $experience)

<div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$experience->date}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->name}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->type}}
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$experience->description}}</div>
            </div>
        </div>
    </div>
@endforeach
@endsection
