<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class CvControlleur extends Controller
{
    //
    public function home()
    {
        $homes = DB::table('homes')->get();
        return view('home', ['homes' => $homes]);

    }
    public function formation()
    {
        $formations = DB::table('formations')->get();
        return view('formation', ['formations' => $formations]);
    }
    public function experience()
    {        
        $experiences = DB::table('experiences')->get();
        return view('experience', ['experiences' => $experiences]);
    }
    public function hobbies()
    {
        $hobbies = DB::table('hobbies')->get();
        return view('hobbies', ['hobbies' => $hobbies]);
    }

}
